package com.boisse.test.testapplication.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

/**
 * A sample annotation that override the {@link RunSpecificBefore} annotation.
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@RunSpecificBefore(testValue = "#{'testValueFromExtendedAnnotation'"
    + " + #RunExtendedSpecificBefore.extendedValue()}")
public @interface RunExtendedSpecificBefore {

  /**
   * The extendedValue attribute.
   */
  @AliasFor("extendedValue")
  String value() default "defaultExtendedValue";

  /**
   * The extendedValue attribute.
   */
  @AliasFor("value")
  String extendedValue() default "defaultExtendedValue";
}
