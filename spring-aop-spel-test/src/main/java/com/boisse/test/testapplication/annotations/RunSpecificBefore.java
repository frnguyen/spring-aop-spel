package com.boisse.test.testapplication.annotations;

import com.boisse.springaopspel.annotations.RunBefore;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

/**
 * A sample annotation that override the {@link RunBefore} annotation.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@RunBefore("@myInteractionService.beforeAction(#args.get(0).getValue() +"
    + " '_' + '#{#RunSpecificBefore.testValue()}')")
public @interface RunSpecificBefore {

  /**
   * The testValue attribute.
   */
  @AliasFor("testValue")
  String value() default "defaultTestValue";

  /**
   * The testValue attribute.
   */
  @AliasFor("value")
  String testValue() default "defaultTestValue";
}
