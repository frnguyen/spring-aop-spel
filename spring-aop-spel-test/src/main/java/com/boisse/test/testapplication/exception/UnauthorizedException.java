package com.boisse.test.testapplication.exception;

import org.springframework.http.HttpStatus;

public class UnauthorizedException extends RuntimeException {

  public UnauthorizedException() {
    super(HttpStatus.UNAUTHORIZED.getReasonPhrase());
  }
}
