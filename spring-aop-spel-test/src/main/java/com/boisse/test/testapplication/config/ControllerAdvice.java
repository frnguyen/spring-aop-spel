package com.boisse.test.testapplication.config;

import com.boisse.test.testapplication.exception.UnauthorizedException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class ControllerAdvice {

  /**
   * Handle {@link UnauthorizedException} exceptions to return a response with 500 status.
   */
  @ExceptionHandler(UnauthorizedException.class)
  public final ResponseEntity handleException(Exception ex, WebRequest request) {
    return ResponseEntity
        .status(HttpStatus.UNAUTHORIZED)
        .body(HttpStatus.UNAUTHORIZED.getReasonPhrase());
  }
}
