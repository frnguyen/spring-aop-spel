package com.boisse.test.testapplication.services;

public interface MyInteractionService {

  void beforeAction(String firstArgument);

  void inAction(String argument);

  void afterReturningAction(String argument);

  void afterThrowingAction(String argument);
}
