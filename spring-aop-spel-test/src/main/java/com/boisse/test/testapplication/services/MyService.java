package com.boisse.test.testapplication.services;

import com.boisse.springaopspel.annotations.RunAfterReturning;
import com.boisse.springaopspel.annotations.RunAfterThrowing;
import com.boisse.springaopspel.annotations.RunBefore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyService {

  private MyInteractionService interactionService;

  @Autowired
  public MyService(MyInteractionService interactionService) {
    this.interactionService = interactionService;
  }

  @RunBefore("@myInteractionService.beforeAction(#args.get(0))")
  @RunAfterReturning("@myInteractionService.afterReturningAction(#args.get(0))")
  @RunAfterThrowing("@myInteractionService.afterThrowingAction(#args.get(0))")
  public void myMethod(String arg) {
    interactionService.inAction(arg);
  }
}
