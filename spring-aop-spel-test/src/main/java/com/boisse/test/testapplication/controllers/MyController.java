package com.boisse.test.testapplication.controllers;

import com.boisse.springaopspel.annotations.RunAfterReturning;
import com.boisse.springaopspel.annotations.RunAfterThrowing;
import com.boisse.springaopspel.annotations.RunBefore;
import com.boisse.test.testapplication.annotations.RunExtendedSpecificBefore;
import com.boisse.test.testapplication.annotations.RunSpecificBefore;
import com.boisse.test.testapplication.controllers.dto.TestDto;
import com.boisse.test.testapplication.services.MyInteractionService;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MyController {

  private MyInteractionService interactioneService;

  @Autowired
  public MyController(MyInteractionService interactioneService) {
    this.interactioneService = interactioneService;
  }

  @PostMapping("/my/before")
  @RunBefore("@myInteractionService.beforeAction(#args.get(0).getValue())")
  @RunSpecificBefore("testValueInController")
  @RunExtendedSpecificBefore("TestValueExtendedInController")
  public ResponseEntity<Void> testRunBefore(@RequestBody @Valid TestDto value) {
    interactioneService.inAction(value.getValue());
    return ResponseEntity.ok().build();
  }


  @PostMapping("/my/afterReturning")
  @RunAfterReturning("@myInteractionService.afterReturningAction(#args.get(0).getValue())")
  public ResponseEntity<Void> testRunAfterReturning(@RequestBody @Valid TestDto value) {
    interactioneService.inAction(value.getValue());
    return ResponseEntity.ok().build();
  }


  @PostMapping("/my/afterThrowing")
  @RunAfterThrowing("@myInteractionService.afterThrowingAction(#args.get(0).getValue())")
  public ResponseEntity<Void> testRunAfterThrowing(@RequestBody @Valid TestDto value) {
    interactioneService.inAction(value.getValue());
    return ResponseEntity.ok().build();
  }
}
