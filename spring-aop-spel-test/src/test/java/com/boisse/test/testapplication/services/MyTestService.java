package com.boisse.test.testapplication.services;

import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.boisse.test.testapplication.exception.UnauthorizedException;
import com.boisse.test.testapplication.utils.InteractionTestUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyTestService extends InteractionTestUtils {

  @Autowired
  private MyService myService;

  @Test
  public void shouldRunAllAction() {

    myService.myMethod("shouldTest");

    verifyInteractions(1, 1, 1, 0);
  }

  @Test
  public void shouldInterceptCallInBeforeAction() {
    mockThrowingBeforeCase();

    assertThatThrownBy(() -> myService.myMethod("shouldTest"))
        .isInstanceOf(UnauthorizedException.class);

    verifyInteractions(1, 0, 0, 0);
  }

}
