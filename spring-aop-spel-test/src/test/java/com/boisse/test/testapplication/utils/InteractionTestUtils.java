package com.boisse.test.testapplication.utils;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.boisse.test.testapplication.exception.UnauthorizedException;
import com.boisse.test.testapplication.services.MyInteractionService;
import org.springframework.boot.test.mock.mockito.MockBean;

public abstract class InteractionTestUtils {

  @MockBean(name = "myInteractionService")
  protected MyInteractionService interactionServiceMocked;


  protected void mockThrowingBeforeCase() {
    doThrow(new UnauthorizedException()).when(interactionServiceMocked).beforeAction(anyString());
  }

  protected void mockThrowingInCase() {
    doThrow(new UnauthorizedException()).when(interactionServiceMocked).inAction(anyString());
  }

  protected void verifyInteractions(int beforeTimes, int inActionTimes, int afterReturningTimes,
      int afterThrowingTimes) {
    verify(interactionServiceMocked, times(beforeTimes)).beforeAction(anyString());
    verify(interactionServiceMocked, times(inActionTimes)).inAction(anyString());
    verify(interactionServiceMocked, times(afterReturningTimes)).afterReturningAction(anyString());
    verify(interactionServiceMocked, times(afterThrowingTimes)).afterThrowingAction(anyString());
  }

}
