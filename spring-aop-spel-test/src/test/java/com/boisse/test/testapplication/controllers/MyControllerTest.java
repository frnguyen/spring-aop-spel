package com.boisse.test.testapplication.controllers;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.boisse.test.testapplication.controllers.dto.TestDto;
import com.boisse.test.testapplication.utils.InteractionTestUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MyControllerTest extends InteractionTestUtils {

  private ObjectMapper mapper = new ObjectMapper();

  @Autowired
  private MockMvc mvc;

  @Test
  public void shouldRunBefore_ok() throws Exception {

    mvc.perform(post("/my/before")
        .accept(MediaType.APPLICATION_JSON)
        .content(mapper.writeValueAsString(TestDto.builder().value("test").build()))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());

    verifyInteractions(3, 1, 0, 0);

    verify(interactionServiceMocked).beforeAction("test" +
        "_" + "testValueInController");

    verify(interactionServiceMocked).beforeAction("test" +
        "_" + "testValueFromExtendedAnnotation" + "TestValueExtendedInController");
  }

  @Test
  public void shouldRunBefore_badRequest() throws Exception {

    mvc.perform(post("/my/before")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));

    verifyInteractions(0, 0, 0, 0);
  }


  @Test
  public void shouldRunBefore_ko() throws Exception {
    mockThrowingBeforeCase();

    mvc.perform(post("/my/before")
        .content(mapper.writeValueAsString(TestDto.builder().value("test").build()))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError())
        .andExpect(content().string(HttpStatus.UNAUTHORIZED.getReasonPhrase()));

    verifyInteractions(1, 0, 0, 0);
  }

  @Test
  public void shouldRunAfterReturning_ok() throws Exception {

    mvc.perform(post("/my/afterReturning")
        .content(mapper.writeValueAsString(TestDto.builder().value("test").build()))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is2xxSuccessful());

    verifyInteractions(0, 1, 1, 0);
  }

  @Test
  public void shouldRunAfterThrowing_ok() throws Exception {
    mockThrowingInCase();

    mvc.perform(post("/my/afterThrowing")
        .content(mapper.writeValueAsString(TestDto.builder().value("test").build()))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().is4xxClientError())
        .andExpect(content().string(HttpStatus.UNAUTHORIZED.getReasonPhrase()));

    verifyInteractions(0, 1, 0, 1);
  }


}
