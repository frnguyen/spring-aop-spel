package com.boisse.springaopspel.support;

import java.lang.annotation.Annotation;
import org.springframework.aop.ClassFilter;
import org.springframework.aop.MethodMatcher;
import org.springframework.aop.Pointcut;
import org.springframework.util.Assert;

/**
 * Based on the spring class
 * {@link org.springframework.aop.support.annotation.AnnotationMatchingPointcut}
 * to use custom {@link RunSpelMethodMatcher}.
 */
public class RunSpelPointcut implements Pointcut {

  private final MethodMatcher methodMatcher;

  protected RunSpelPointcut(Class<? extends Annotation> methodAnnotationType) {
    if (methodAnnotationType != null) {
      this.methodMatcher = RunSpelMethodMatcher.forMethodAnnotation(methodAnnotationType);
    } else {
      this.methodMatcher = MethodMatcher.TRUE;
    }
  }

  @Override
  public ClassFilter getClassFilter() {
    return ClassFilter.TRUE;
  }

  @Override
  public MethodMatcher getMethodMatcher() {
    return this.methodMatcher;
  }


  /**
   * Factory method for an {@link RunSpelPointcut} that matches for the specified annotation at the
   * method level.
   *
   * @param annotationType the annotation type to look for at the method level.
   * @return the corresponding AnnotationMatchingPointcut.
   */
  public static RunSpelPointcut forMethodAnnotation(
      Class<? extends Annotation> annotationType) {
    Assert.notNull(annotationType, "Annotation type must not be null");
    return new RunSpelPointcut(annotationType);
  }

}
