package com.boisse.springaopspel.support;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import org.springframework.aop.support.StaticMethodMatcher;
import org.springframework.aop.support.annotation.AnnotationMethodMatcher;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.Assert;

/**
 * Based on the spring class {@link AnnotationMethodMatcher} to make a check if method has
 * annotation with {@link AnnotatedElementUtils}.
 */
public class RunSpelMethodMatcher extends StaticMethodMatcher {

  private final Class<? extends Annotation> annotationType;

  protected RunSpelMethodMatcher(Class<? extends Annotation> annotationType) {
    this.annotationType = annotationType;
  }

  @Override
  public boolean matches(Method method, Class<?> targetClass) {
    return AnnotatedElementUtils.hasAnnotation(method, annotationType);
  }

  /**
   * Factory method for an {@link RunSpelMethodMatcher} that matches for the specified annotation at
   * the method level.
   *
   * @param annotationType the annotation type to look for at the method level.
   * @return the corresponding RunSpelMethodMatcher.
   */
  public static RunSpelMethodMatcher forMethodAnnotation(
      Class<? extends Annotation> annotationType) {
    Assert.notNull(annotationType, "Annotation type must not be null");
    return new RunSpelMethodMatcher(annotationType);
  }
}
