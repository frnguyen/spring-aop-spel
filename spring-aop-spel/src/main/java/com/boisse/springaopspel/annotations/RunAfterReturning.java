package com.boisse.springaopspel.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

/**
 * Run a SpEL expression after returning a method call.
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface RunAfterReturning {

  /**
   * The SpEL expression to run. (This is an alias of field spelRexpression).
   */
  @AliasFor("spelExpression")
  String value() default "";

  /**
   * The SpEL expression to run.
   */
  @AliasFor("value")
  String spelExpression() default "";
}
