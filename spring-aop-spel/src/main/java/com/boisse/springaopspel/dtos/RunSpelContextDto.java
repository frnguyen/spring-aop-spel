package com.boisse.springaopspel.dtos;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.ApplicationContext;

public class RunSpelContextDto<T extends Annotation> {

  private T runSpelAnnotation;

  private Method method;

  private List<Object> args;

  private Exception exception;

  private Object returnValue;

  private List<Annotation> extendedAnnotations;

  private ApplicationContext applicationContext;

  public T getRunSpelAnnotation() {
    return runSpelAnnotation;
  }

  public void setRunSpelAnnotation(T runSpelAnnotation) {
    this.runSpelAnnotation = runSpelAnnotation;
  }

  public Method getMethod() {
    return method;
  }

  public void setMethod(Method method) {
    this.method = method;
  }

  /**
   * Get the args.
   */
  public List<Object> getArgs() {
    if (args == null) {
      args = new ArrayList<>();
    }
    return args;
  }

  public void setArgs(List<Object> args) {
    this.args = args;
  }

  public Exception getException() {
    return exception;
  }

  public void setException(Exception exception) {
    this.exception = exception;
  }

  public Object getReturnValue() {
    return returnValue;
  }

  public void setReturnValue(Object returnValue) {
    this.returnValue = returnValue;
  }

  /**
   * Get the annotations.
   */
  public List<Annotation> getExtendedAnnotations() {
    if (extendedAnnotations == null) {
      extendedAnnotations = new ArrayList<>();
    }
    return extendedAnnotations;
  }

  public void setExtendedAnnotations(List<Annotation> extendedAnnotations) {
    this.extendedAnnotations = extendedAnnotations;
  }

  public ApplicationContext getApplicationContext() {
    return applicationContext;
  }

  public void setApplicationContext(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }
}
