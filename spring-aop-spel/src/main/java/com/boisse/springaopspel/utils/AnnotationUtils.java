package com.boisse.springaopspel.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.CollectionUtils;

public abstract class AnnotationUtils {

  /**
   * Retrieve the intermediate annotations between an baseAnnotation and the targetAnnotation.
   *
   * @param baseAnnotation The base annotation where start the hierarchy.
   * @param targetAnnotation The target annotation where stop the hierarchy.
   * @param <T> The annotation to search for.
   * @return The list of all annotations between the base and the target annotations.
   */
  public static <T extends Annotation> List<Annotation> getHierarchyAnnotations(
      Annotation baseAnnotation, Class<T> targetAnnotation) {
    return getHierarchyAnnotations(Collections.singletonList(baseAnnotation),
        targetAnnotation);
  }

  private static <T extends Annotation> List<Annotation> getHierarchyAnnotations(
      List<Annotation> baseAnnotations, Class<T> targetAnnotation) {
    if (CollectionUtils.isEmpty(baseAnnotations)) {
      return new ArrayList<>();
    }

    return baseAnnotations.stream()
        .filter(ann -> AnnotatedElementUtils.hasAnnotation(ann.annotationType(), targetAnnotation))
        .flatMap(annotation -> {
          List<Annotation> runBeforeAnnotations = new ArrayList<>();

          runBeforeAnnotations.add(annotation);

          List<Annotation> subAnnotations = getAnnotations(annotation.annotationType());
          runBeforeAnnotations.addAll(
              getHierarchyAnnotations(subAnnotations, targetAnnotation));

          return runBeforeAnnotations.stream();
        })
        .collect(Collectors.toList());
  }

  /**
   * Retrieve the annotations present on the annotatedElement.
   *
   * @param annotatedElement The annotatedElement where search annotations.
   * @return The list of annotations present on the annotatedElement.
   */
  public static List<Annotation> getAnnotations(AnnotatedElement annotatedElement) {
    return Arrays.asList(
        org.springframework.core.annotation.AnnotationUtils.getAnnotations(annotatedElement)
    );
  }
}
