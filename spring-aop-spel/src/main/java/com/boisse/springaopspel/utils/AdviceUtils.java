package com.boisse.springaopspel.utils;

import com.boisse.springaopspel.dtos.RunSpelContextDto;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.core.annotation.AnnotatedElementUtils;

public abstract class AdviceUtils {

  /**
   * Return the list of annotations of the parameterized type.
   *
   * @param annotatedElement The annotated element
   * @param annotationClass The annotation to search
   * @return The list of annotation of the asked type.
   */
  public static Set<Annotation> getAnnotations(AnnotatedElement annotatedElement,
      Class<? extends Annotation> annotationClass) {

    return AnnotationUtils.getAnnotations(annotatedElement).stream()
        // Filter to keep annotation of the asked type
        .filter(ann -> ann.annotationType().isAssignableFrom(annotationClass)
            // Or annotations that own the asked type
            || AnnotatedElementUtils.hasAnnotation(ann.annotationType(), annotationClass))
        .collect(Collectors.toSet());
  }

  /**
   * Build the RunSpel Context DTO.
   *
   * @param annotation The annotation to build teh context for.
   * @param runSpelType The runSpel annotation to search for.
   * @param <T> The type of runSpel annotation.
   * @return The runSpelContextDto.
   */
  public static <T extends Annotation> RunSpelContextDto<T> buildRunSpelContext(
      Annotation annotation, Class<T> runSpelType) {

    RunSpelContextDto<T> runSpelContext = new RunSpelContextDto<>();

    if (annotation.annotationType().isAssignableFrom(runSpelType)) {
      // The annotation is the asked annotation
      runSpelContext.setRunSpelAnnotation(runSpelType.cast(annotation));

      return runSpelContext;

    } else if (AnnotatedElementUtils.hasAnnotation(annotation.annotationType(), runSpelType)) {
      // The annotation own the asked annotation
      runSpelContext.setRunSpelAnnotation(
          // Search for the asked annotation
          AnnotatedElementUtils.getMergedAnnotation(annotation.annotationType(), runSpelType)
      );
      runSpelContext.setExtendedAnnotations(
          // Get all annotations that extend the asked annotation
          AnnotationUtils.getHierarchyAnnotations(annotation, runSpelType)
      );

      return runSpelContext;

    } else {
      return null;
    }
  }
}
