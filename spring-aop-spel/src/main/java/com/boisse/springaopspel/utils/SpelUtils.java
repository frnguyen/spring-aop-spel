package com.boisse.springaopspel.utils;

import com.boisse.springaopspel.dtos.RunSpelContextDto;
import org.springframework.context.expression.BeanFactoryResolver;
import org.springframework.expression.Expression;
import org.springframework.expression.ParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.StringUtils;

public abstract class SpelUtils {

  private static final String IS_TEMPLATE_MATCHER = ".*#\\{.+}.*";

  /**
   * Util method to execute a SpEL expressions.
   */
  public static Object runSpel(String spelExpression, StandardEvaluationContext ctx) {
    if (StringUtils.isEmpty(spelExpression)) {
      return null;
    }

    Expression expression = new SpelExpressionParser()
        .parseExpression(spelExpression, detectParserContext(spelExpression));

    Object result = expression.getValue(ctx);
    if (result instanceof String) {
      return runSpel((String) result, ctx);
    }
    return result;
  }

  private static ParserContext detectParserContext(String spelExpression) {
    boolean isTemplate = spelExpression.matches(IS_TEMPLATE_MATCHER);
    return isTemplate ? ParserContext.TEMPLATE_EXPRESSION : null;
  }

  /**
   * Build a SpEL evaluation context from the runSpelContext Dto.
   *
   * @param runSpelContext the runSpelContextDto.
   * @return The SpEL evaluation context.
   */
  public static StandardEvaluationContext buildEvaluationContext(
      RunSpelContextDto<?> runSpelContext) {

    StandardEvaluationContext context = new StandardEvaluationContext();
    context.setVariable("args", runSpelContext.getArgs());
    context.setVariable("method", runSpelContext.getMethod());
    if (runSpelContext.getException() != null) {
      context.setVariable("exception", runSpelContext.getException());
    }
    if (runSpelContext.getReturnValue() != null) {
      context.setVariable("returnValue", runSpelContext.getReturnValue());
    }

    context.setBeanResolver(new BeanFactoryResolver(runSpelContext.getApplicationContext()));

    runSpelContext.getExtendedAnnotations().forEach(
        annotation -> context.setVariable(annotation.annotationType().getSimpleName(), annotation)
    );

    return context;
  }
}
