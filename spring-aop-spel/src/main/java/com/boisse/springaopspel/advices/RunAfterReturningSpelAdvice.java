package com.boisse.springaopspel.advices;

import com.boisse.springaopspel.annotations.RunAfterReturning;
import com.boisse.springaopspel.dtos.RunSpelContextDto;
import com.boisse.springaopspel.utils.AdviceUtils;
import com.boisse.springaopspel.utils.SpelUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import org.springframework.aop.AfterReturningAdvice;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.CollectionUtils;

public class RunAfterReturningSpelAdvice implements AfterReturningAdvice {

  protected ApplicationContext applicationContext;

  public RunAfterReturningSpelAdvice(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * The run aspect for {@link RunAfterReturning}.
   */
  @Override
  public void afterReturning(Object returnValue, Method method, Object[] args, Object target) {

    Set<Annotation> runBeforeAnnotations =
        AdviceUtils.getAnnotations(method, RunAfterReturning.class);

    if (!CollectionUtils.isEmpty(runBeforeAnnotations)) {
      for (Annotation annotation : runBeforeAnnotations) {

        RunSpelContextDto<RunAfterReturning> runSpelContext =
            AdviceUtils.buildRunSpelContext(annotation, RunAfterReturning.class);

        if (runSpelContext != null) {
          // Set the RunAfterReturning context
          runSpelContext.setMethod(method);
          runSpelContext.setArgs(Arrays.asList(args));
          runSpelContext.setReturnValue(returnValue);
          runSpelContext.setApplicationContext(applicationContext);

          String spelExpression = runSpelContext.getRunSpelAnnotation().spelExpression();
          StandardEvaluationContext ctx = SpelUtils.buildEvaluationContext(runSpelContext);
          SpelUtils.runSpel(spelExpression, ctx);
        }
      }
    }
  }

}
