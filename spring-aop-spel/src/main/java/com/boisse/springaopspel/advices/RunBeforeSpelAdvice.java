package com.boisse.springaopspel.advices;

import com.boisse.springaopspel.annotations.RunBefore;
import com.boisse.springaopspel.dtos.RunSpelContextDto;
import com.boisse.springaopspel.utils.AdviceUtils;
import com.boisse.springaopspel.utils.SpelUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.CollectionUtils;

public class RunBeforeSpelAdvice implements MethodBeforeAdvice {

  protected ApplicationContext applicationContext;

  public RunBeforeSpelAdvice(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * The run aspect for {@link RunBefore}.
   */
  @Override
  public void before(Method method, Object[] args, Object target) {

    Set<Annotation> runBeforeAnnotations = AdviceUtils.getAnnotations(method, RunBefore.class);

    if (!CollectionUtils.isEmpty(runBeforeAnnotations)) {
      for (Annotation annotation : runBeforeAnnotations) {

        RunSpelContextDto<RunBefore> runSpelContext =
            AdviceUtils.buildRunSpelContext(annotation, RunBefore.class);

        if (runSpelContext != null) {
          // Set the RunBefore context
          runSpelContext.setMethod(method);
          runSpelContext.setArgs(Arrays.asList(args));
          runSpelContext.setApplicationContext(applicationContext);

          String spelExpression = runSpelContext.getRunSpelAnnotation().spelExpression();
          StandardEvaluationContext ctx = SpelUtils.buildEvaluationContext(runSpelContext);
          SpelUtils.runSpel(spelExpression, ctx);
        }
      }
    }
  }


}
