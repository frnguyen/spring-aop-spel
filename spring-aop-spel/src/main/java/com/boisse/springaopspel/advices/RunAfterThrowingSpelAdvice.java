package com.boisse.springaopspel.advices;

import com.boisse.springaopspel.annotations.RunAfterThrowing;
import com.boisse.springaopspel.dtos.RunSpelContextDto;
import com.boisse.springaopspel.utils.AdviceUtils;
import com.boisse.springaopspel.utils.SpelUtils;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Set;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.CollectionUtils;

public class RunAfterThrowingSpelAdvice implements ThrowsAdvice {

  protected ApplicationContext applicationContext;

  public RunAfterThrowingSpelAdvice(ApplicationContext applicationContext) {
    this.applicationContext = applicationContext;
  }

  /**
   * The run aspect for {@link RunAfterThrowing}.
   */
  public void afterThrowing(Method method, Object[] args, Object target, Exception exception) {

    Set<Annotation> runBeforeAnnotations =
        AdviceUtils.getAnnotations(method, RunAfterThrowing.class);

    if (!CollectionUtils.isEmpty(runBeforeAnnotations)) {
      for (Annotation annotation : runBeforeAnnotations) {

        RunSpelContextDto<RunAfterThrowing> runSpelContext =
            AdviceUtils.buildRunSpelContext(annotation, RunAfterThrowing.class);

        if (runSpelContext != null) {
          // Set the RunAfterThrowing context
          runSpelContext.setMethod(method);
          runSpelContext.setArgs(Arrays.asList(args));
          runSpelContext.setException(exception);
          runSpelContext.setApplicationContext(applicationContext);

          String spelExpression = runSpelContext.getRunSpelAnnotation().spelExpression();
          StandardEvaluationContext ctx = SpelUtils.buildEvaluationContext(runSpelContext);
          SpelUtils.runSpel(spelExpression, ctx);
        }
      }
    }
  }

}
