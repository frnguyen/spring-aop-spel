package com.boisse.springaopspel;

import com.boisse.springaopspel.advices.RunAfterReturningSpelAdvice;
import com.boisse.springaopspel.advices.RunAfterThrowingSpelAdvice;
import com.boisse.springaopspel.advices.RunBeforeSpelAdvice;
import com.boisse.springaopspel.annotations.RunAfterReturning;
import com.boisse.springaopspel.annotations.RunAfterThrowing;
import com.boisse.springaopspel.annotations.RunBefore;
import com.boisse.springaopspel.support.RunSpelPointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration class for bean declaration.
 */
@Configuration
@ComponentScan
public class LibConfig {

  /**
   * The {@link ProxyFactoryBean} bean to create automatically a proxy for all {@link Advisor}
   * beans.
   */
  @Bean
  public ProxyFactoryBean proxyFactoryBean() {
    return new ProxyFactoryBean();
  }

  /**
   * The {@link Advisor} bean for {@link RunBefore}.
   */
  @Bean
  public Advisor runBeforeAdvisor(ApplicationContext applicationContext) {
    return new DefaultPointcutAdvisor(
        RunSpelPointcut.forMethodAnnotation(RunBefore.class),
        new RunBeforeSpelAdvice(applicationContext));
  }

  /**
   * The {@link Advisor} bean for {@link RunAfterReturning}.
   */
  @Bean
  public Advisor runAfterReturningAdvisor(ApplicationContext applicationContext) {
    return new DefaultPointcutAdvisor(
        RunSpelPointcut.forMethodAnnotation(RunAfterReturning.class),
        new RunAfterReturningSpelAdvice(applicationContext));
  }

  /**
   * The {@link Advisor} bean for {@link RunAfterThrowing}.
   */
  @Bean
  public Advisor runAfterThrowingAdvisor(ApplicationContext applicationContext) {
    return new DefaultPointcutAdvisor(
        RunSpelPointcut.forMethodAnnotation(RunAfterThrowing.class),
        new RunAfterThrowingSpelAdvice(applicationContext));
  }

}
