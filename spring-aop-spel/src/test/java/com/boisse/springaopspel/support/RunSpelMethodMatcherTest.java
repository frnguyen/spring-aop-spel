package com.boisse.springaopspel.support;

import static org.assertj.core.api.Assertions.assertThat;

import com.boisse.springaopspel.annotations.RunBefore;
import com.boisse.springaopspel.support.sample.ClassSample;
import com.boisse.springaopspel.support.sample.InterfaceSample;
import com.boisse.springaopspel.support.sample.impl.InterfaceSampleImpl;
import java.lang.reflect.Method;
import org.junit.Test;

public class RunSpelMethodMatcherTest {

  private RunSpelMethodMatcher matcher =
    RunSpelMethodMatcher.forMethodAnnotation(RunBefore.class);

  @Test
  public void shouldMatchOnInterface() throws Exception {
    Method method = InterfaceSampleImpl.class.getMethod("runBefore");
    assertThat(matcher.matches(method, null)).isTrue();

    Method methodInterface = InterfaceSample.class.getMethod("runBefore");
    assertThat(matcher.matches(methodInterface, null)).isTrue();
  }

  @Test
  public void shouldMatchOnClass() throws Exception {
    Method method = ClassSample.class.getMethod("runBefore");
    assertThat(matcher.matches(method, null)).isTrue();
  }

  @Test
  public void shouldNotMatch_WrongAnnotation() throws Exception {
    Method method = ClassSample.class.getMethod("runAfterReturning");
    assertThat(matcher.matches(method, null)).isFalse();
  }

  @Test
  public void shouldNotMatch_NoAnnotation() throws Exception {
    Method method = ClassSample.class.getMethod("noAnnotation");
    assertThat(matcher.matches(method, null)).isFalse();
  }
}
