package com.boisse.springaopspel.support.sample;

import com.boisse.springaopspel.annotations.RunBefore;

public interface InterfaceSample {

  @RunBefore
  void runBefore();

}
