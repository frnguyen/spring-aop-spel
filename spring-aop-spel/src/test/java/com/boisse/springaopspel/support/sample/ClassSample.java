package com.boisse.springaopspel.support.sample;

import com.boisse.springaopspel.annotations.RunAfterReturning;
import com.boisse.springaopspel.annotations.RunBefore;

public class ClassSample {

  @RunBefore
  public void runBefore() {

  }

  public void noAnnotation() {

  }

  @RunAfterReturning
  public void runAfterReturning() {

  }
}
