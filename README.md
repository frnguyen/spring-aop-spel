# Spring-aop-spel

[![pipeline status](https://gitlab.com/fboisselier52/spring-aop-spel/badges/master/pipeline.svg)](https://gitlab.com/fboisselier52/spring-aop-spel/commits/master)

This module provides annotations to execute code around function calls.
The executed code is declared with a Spring Expression Language (SpEL).

## Add dependency

Include the dependency to your app:

**Maven**:

```xml
<dependencies>
  ...
  <dependency>
    <groupId>com.boisse</groupId>
    <artifactId>spring-aop-spel</artifactId>
    <version>1.0-SNAPSHOT</version>
  </dependency>
  ...
</dependencies>
```

## Requirements

The `spring-aop-spel` extension depend on `spring-boot`.

## Usage

Add `RunBefore`, `RunAfter`, `RunAfterReturning`, `RunAfterThrowing` annotations to your methods :

``` java
@RunBefore("@myInterceptorBean.beforeAction(#args.get(0))")
@RunAfterReturning("@myInterceptorBean.afterReturningAction(#args.get(0))")
@RunAfterThrowing("@myInterceptorBean.afterThrowingAction(#args.get(0))")
public void myMethod(String arg) {
  // your code
}
```

### Annotations

* `RunBefore` : Handled before the method is called.
* `RunAfterReturning` : Handled after the method is called. Only when the method returns.
* `RunAfterThrowing` : Handled after the method is called. Only when the method throw an exception.

### SpEL expression

See [Spring Documentation](https://docs.spring.io/spring/docs/5.1.5.RELEASE/spring-framework-reference/core.html#expressions-language-ref)

The aspect injects in SpEL context :
* `#method` : The invoked method
* `#args`: The arguments list to the invoked method
* `#returnValue` : The value returned by the method, if any - Only for `@RunAfterReturning`.
* `#exception` : The exception thrown - Only for `@RunAfterThrowing`.
* The spring context

### Make your custom annotations

#### Override the default annotations
You can define your own annotations. The annotation is injected in the SpEL context. So their properties are accessible in the SpEL expression.
 
See an example :

``` java
@Target({ElementType.METHOD}) // Mandatory
@Retention(RetentionPolicy.RUNTIME) // Mandatory
@RunBefore("@myInteractionService.beforeAction(#args.get(0).getValue() + '#{#MyBeforeAnnotation.customProperty()}')")
public @interface MyBeforeAnnotation {
  
  @AliasFor("customProperty")
  String value();

  @AliasFor("value")
  String customProperty();
}
```

And use it on your function :

``` java
@MyBeforeAnnotation("myCustomProperty")
public void myMethod(String arg) {
  // your code
}
```

#### Inheritance mechanism

You can use the SpEL template notation (`'#{#MyBeforeAnnotation.customProperty()}'`) to make your annotations inheritable.

With this notation, you can define another annotation :

``` java
@Target({ElementType.METHOD}) // Mandatory
@Retention(RetentionPolicy.RUNTIME) // Mandatory
@MyBeforeAnnotation("#{'prefix_' + #MyExtendedBeforeAnnotation.extendedCustomProperty() + '_suffix'}")
public @interface MyExtendedBeforeAnnotation {
  
  @AliasFor("extendedCustomProperty")
  String value() default "extendedValue";

  @AliasFor("value")
  String extendedCustomProperty() default "extendedValue";
}
```

The SpEL parser will evaluate :
* Firstly : `@myInteractionService.beforeAction(#args.get(0).getValue() + '#{#MyBeforeAnnotation.customProperty()}')`
* Next : `@myInteractionService.beforeAction(#args.get(0).getValue() + '#{'prefix_' + #MyExtendedBeforeAnnotation.extendedCustomProperty() + '_suffix'}')`
* Finally : `@myInteractionService.beforeAction(#args.get(0).getValue() + 'prefix_extendedValue_suffix')`